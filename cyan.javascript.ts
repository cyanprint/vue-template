import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, Glob, IAutoInquire, IAutoMapper, IExecute} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {

    let useWebStorm = await autoInquirer.InquirePredicate("Do you use WebStorm?");

    let globs: Glob[] = [{root: "./Template/Common/", pattern: "**/*.*", ignore: []}];
    let guid: string[] = [];
    let NPM: string | boolean = true;

    let basicFolder: Glob = {root: "./Template/Javascript", pattern: "**/*.*", ignore: []};
    let additionalFolder: Glob = {root: "./Template/Javascript", pattern: "**/*", ignore: []};
    if (!useWebStorm) (basicFolder.ignore as string[]).push("**/.idea/**/*");
    globs.push(basicFolder, additionalFolder);

    //Ask for documentation
    let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
    let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);

    //Get read question to ask
    let variables: any = {
        name: {
            package: ["vue-frontend", "Enter the name of package.json"]
        }
    };

    //Get answers
    variables = await autoInquirer.InquireInput(variables);

    //Fill in empty information if document already have them
    variables.name.author = docs.data.author;
    variables.name.project = docs.data.projectName;
    variables.email = docs.data.email;
    variables.description = docs.data.description;

    let flags: any = {
        rimage: {
            bucket: false,
            pngquant: false,
            jpegoptim: false,
            wuquant: false
        },
        gitlab: false,
        unitTest: false,
        pwa: false,
        packages: {
            //babel
            "@babel/core": false,
            "@babel/plugin-proposal-object-rest-spread": false,
            "@babel/preset-env": false,
            "babel-loader": false,
            //tools
            lodash: false,
            "@kirinnee/objex": false,
            "@kirinnee/rimage": false,
            vuex: false,
            "vue-router": false,
            //e2e
            testcafe: false,
            //test
            chai: false,
            mocha: false,
            jest: false,
            nyc: false,
            //polyfill
            "@babel/polyfill": false,
            "whatwg-fetch": false,
            "dom4": false,
            //sass
            "node-sass": false,
            "sass-loader": false,
            //style
            "style-loader": false,
            "css-loader": false,
            "ignore-styles": false,
            "identity-obj-proxy": false,
            //minify
            "terser-webpack-plugin": false,
            "uglifyjs-webpack-plugin": true,
            //postcss
            "postcss-loader": false,
            "postcss-preset-env": false,

            // pwa
            "workbox-webpack-plugin": false,
        },
        move: {
            "@kirinnee/core": false
        },
        polyfill: false,
        cover: false,
        build: {
            babel: false,
            style: false,
            sass: false,
            postcss: false,
            sourceMap: true,
            es6: false,
            console: false,
            unsafe: false,
            stripComment: false,
            renameGlobal: false,
            hmr: false,
            split: false,
            includeVueCompiler: false
        },
        e2e: {
            chrome: {
                headless: false,
                real: false
            },
            firefox: {
                headless: false,
                real: false
            },
            "chrome-canary": {
                headless: false,
                real: false
            },
            ie: false,
            opera: false,
            edge: false
        }
    };

    //Ask for for features
    let features: any = {
        unitTest: "Unit Test",
        polyfill: "Polyfills",
        pwa: "Progressive Web App",
        packages: {
            lodash: "Lodash",
            "@kirinnee/objex": "Objex",
            "vuex": "VueX",
            "vue-router": "Vue Router",
            testcafe: "E2E (TestCafe Only)",
            "@kirinnee/rimage": "rImage",
        },
        move: {
            "@kirinnee/core": "Kirinnee Core Library"
        },
        build: {
            style: "StyleSheet"
        }

    };

    features = await autoInquirer.InquireAsCheckBox(features, "Which of these tools do you need?");
    flags = autoMap.Overwrite(features, flags);

    //if unit test
    if (flags.unitTest) {

        let coverage = await autoInquirer.InquirePredicate("Do you need coverage tool?");
        flags.cover = coverage;

        let mochaChai: any = {
            packages: {
                chai: true,
                mocha: true,
                nyc: coverage,
                rimraf: coverage,
            },
        };
        let jest: any = {
            packages: {
                jest: true
            },
        };
        let test: boolean = await autoInquirer.InquirePredicate("Which Test Framework do you want to use?", "Mocha + Chai", "Jest");
        flags = autoMap.Overwrite(test ? mochaChai : jest, flags);

    }

    delete flags.unitTest;

    //if polyfill wanted
    if (flags.polyfill) {
        //Ask for polyfill
        let polyfill: any = {
            packages: {
                "@babel/polyfill": "Babel Polyfill",
                "whatwg-fetch": "Fetch API Polyfill",
                "dom4": "DOM4 Polyfill"
            }
        };

        polyfill = await autoInquirer.InquireAsCheckBox(polyfill, "Which do you this poly-fill do you need for testing?");
        polyfill.polyfill = polyfill.packages["@babel/polyfill"] || polyfill.packages["whatwg-fetch"] || polyfill.packages["dom4"];
        flags = autoMap.Overwrite(polyfill, flags);
    }

    // ask for browser testing
    if (flags.packages.testcafe) {
        let headless: any = {
            e2e: {
                chrome: {
                    headless: "Google Chrome"
                },
                firefox: {
                    headless: "Firefox"
                },
                "chrome-canary": {
                    headless: "Google Chrome Canary"
                }
            }
        };
        headless = await autoInquirer.InquireAsCheckBox(headless, "Which of these headless browser do you want to use for E2E Tests?");

        let full: any = {
            e2e: {
                chrome: {
                    real: "Google Chrome"
                },
                "chrome-canary": {
                    real: "Google Chrome Canary"
                },
                firefox: {
                    real: "Firefox"
                },
                ie: "Internet Explorer",
                opera: "Opera",

                edge: "Microsoft Edge"
            }
        };
        full = await autoInquirer.InquireAsCheckBox(full, "Which of these real browsers do you want to use for full E2E Tests?");
        let total: any = autoMap.JoinObjects(full, headless);
        flags = autoMap.Overwrite(total, flags);
    }

    //Ask for build pipeline
    let builds: any = {
        build: {
            babel: "Babel 7",
            sourceMap: "Source Map",
            es6: "Minify ES6 (Use Terser instead of Uglify)",
            console: "Keep console.log",
            unsafe: "Unsafe Compression",
            stripComment: "Strip Comments",
            renameGlobal: "Top Level Mangle",
            hmr: "Hot Module Reloading",
            split: "Chunk Splitting",
            includeVueCompiler: "Use Vue Compiler with Runtime",
            sass: "SASS",
            postcss: "PostCSS"
        }
    };

    builds = await autoInquirer.InquireAsCheckBox(builds, "Which of this features do you want in your build pipeline?");
    flags = autoMap.Overwrite(builds, flags);

    if (flags.pwa) {
        flags.packages["workbox-webpack-plugin"] = true;
        const x: any = await autoInquirer.InquireInput({
            shortName: ["CyanPrint", "PWA Application Name"],
            backgroundColor: ["#3367D6", "PWA Background Color"],
            themeColor: ["#3367D6", "PWA Theme Color"],
        });
        variables.name.short = x.shortName;
        variables.color = {};
        variables.color.background = x.backgroundColor;
        variables.color.theme = x.themeColor;
    }


    flags.gitlab = await autoInquirer.InquirePredicate("Use GitLab CI to deploy to Netlify? (Provide Netlify key and token via Variables)");

    // Ask for rImage settings
    if (flags.packages["@kirinnee/rimage"]) {
        flags.rimage.bucket = await autoInquirer.InquirePredicate("Deploy to bucket or local disk?", "Bucket", "Local disk");
        let rimageOptions: any = {
            rimage: {
                pngquant: "PngQuant (PNG Lossy Compression)",
                jpegoptim: "JPEGOptim (JPEG Lossy Compression)",
                wuquant: "WuQuant (Fast PNG quantization (Lossy Compression)"
            }
        };
        rimageOptions = await autoInquirer.InquireAsCheckBox(rimageOptions, "Which compression on the image do you want to use?");
        flags = autoMap.Overwrite(rimageOptions, flags);

        if (flags.rimage.bucket) {
            variables.rimage = await autoInquirer.InquireInput({deploy: ["bucket.name", "Enter the bucket name"]});
            const providers = {
                aws: "Amazon Web Service",
                azure: "Azure Blob Storage",
                gcp: "Google Cloud Platform",
                do: "Digital Ocean Spaces",
            };
            const result = await autoInquirer.InquireAsList(providers, "Which provider do you want to use?");
            const answer = autoMap.ReverseLoopUp(providers, result);
            if (answer === providers.aws) {
                const r: any = await autoInquirer.InquireInput({region: ["ap-southeast-1", "Which AWS region are you using?"]});
                variables.rimage.provider = "aws";
                variables.rimage.publicPath = `https://s3-${r.region}.amazonaws.com/${variables.rimage.deploy}/`;
                variables.rimage.arguments = "--accessId=$AWS_ACCESS_ID --accessSecret=$AWS_SECRET --region=$AWS_REGION";
            } else if (answer === providers.azure) {
                const r: any = await autoInquirer.InquireInput({acc: ["accountname", "Enter your storage account name (find in AzurePortal > Access keys > storage account name)"]});
                variables.rimage.provider = "azure";
                variables.rimage.publicPath = `https://${r.acc}.blob.core.windows.net/${variables.rimage.deploy}/`;
                variables.rimage.arguments = "--accountName=$AZURE_ACCOUNT_NAME --accessToken=$AZURE_ACCESS_TOKEN";
            } else if (answer === providers.gcp) {
                variables.rimage.provider = "gcp";
                variables.rimage.publicPath = `https://storage.googleapis.com/${variables.rimage.deploy}/`;
                variables.rimage.arguments = "--projectID=$GCP_PROJECT_ID --jsonSecret=$GCP_JSON_SECRET";
            } else if (answer === providers.do) {
                const r: any = await autoInquirer.InquireInput({region: ["sgp1", "Which DigitalOcean region is your space in?"]});
                variables.rimage.provider = "do";
                variables.rimage.publicPath = `https://${r.region}.digitaloceanspaces.com/${variables.rimage.deploy}/`;
                variables.rimage.arguments = "--accessId=$DO_ACCESS_ID --accessSecret=$DO_SECRET --region=$DO_REGION";
            }
        } else {
            variables.rimage = {deploy: '../dist/pages/'};
            variables.rimage.provider = "none";
            variables.rimage.publicPath = '/';
            variables.rimage.arguments = "none";
        }
    }


    //Consolidate flags
    if (flags.build.sass) {
        flags.packages["sass-loader"] = true;
        flags.packages["node-sass"] = true;
    }

    if (flags.build.style) {
        flags.packages["style-loader"] = true;
        flags.packages["css-loader"] = true;
        if (flags.packages.chai) {
            flags.packages["ignore-styles"] = true
        }
        if (flags.packages.jest) {
            flags.packages["identity-obj-proxy"] = true;
        }
    }


    if (flags.build.es6) {
        flags.packages["uglifyjs-webpack-plugin"] = false;
        flags.packages["terser-webpack-plugin"] = true;
    }

    let browserlist: boolean = false;

    if (flags.build.postcss) {
        flags.packages["postcss-loader"] = true;
        flags.packages["postcss-preset-env"] = true;
        browserlist = true;
    } else {
        globs = globs.map(g => {
            g.ignore = (g.ignore as string[]).concat(["**/postcss.config.js"]);
            return g;
        });
    }

    if (flags.build.babel) {
        flags.packages["@babel/core"] = true;
        flags.packages["babel-loader"] = true;
        flags.packages["@babel/preset-env"] = true;
        flags.packages["@babel/plugin-proposal-object-rest-spread"] = true;
        browserlist = true;
    }

    if (!browserlist) {
        globs = globs.map(g => {
            g.ignore = (g.ignore as string[]).concat(["**/.browserslistrc"]);
            return g;
        });
    }

    if (!flags.packages["@kirinnee/rimage"]) {
        globs = globs.map(g => {
            g.ignore = (g.ignore as string[]).concat(["**/image.deploy.config.json"]);
            return g;
        });
    }

    if (docs.usage.git) variables.git = docs.data.gitURL;

    if (flags.gitlab) globs.push({
        root: "./Template",
        pattern: ".gitlab-ci.yml",
        ignore: ["**/Javascript/**/*", "**/Typescript/**/*", "**/Common/**/*"]
    });

    return {
        globs: globs,
        flags: flags,
        variable: variables,
        docs: docs,
        guid: guid,
        comments: ["//", "#"],
        npm: NPM
    } as Cyan;

}
