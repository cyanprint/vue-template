# var~cyan.docs.name~

var~cyan.docs.description~

# Getting Started

## Development
To run a local dev server
```bash
$ npm run dev
```
or 
```bash
$ yarn dev
```
## Unit Test
Run unit test
```bash
$ npm run test
```
or
```bash
$ yarn test
```

## 


if~cyan.docs.contributing~	
## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.MD) for details on our code of conduct, and the process for submitting pull requests to us.
end~cyan.docs.contributing~	

if~cyan.docs.semver~
## Versioning 
We use [SemVer](https://semver.org/) for versioning. For the versions available, see the tags on this repository.
end~cyan.docs.semver~

## Authors
* [var~cyan.docs.author~](mailto:var~cyan.docs.email~) 

if~cyan.docs.license~
## License
This project is licensed under var~cyan.docs.license~ - see the [LICENSE.md](LICENSE.MD) file for details
end~cyan.docs.license~