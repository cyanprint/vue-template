import {Auth0DecodedHash, Auth0Error, Auth0ParseHashError, WebAuth} from 'auth0-js';
import EventEmitter from 'events';
import {ProfileData} from "./ProfileData";

class AuthService extends EventEmitter {
	
	private readonly localStorageKey: string = 'loggedIn';
	private readonly loginEvent: string = 'loginEvent';
	private readonly auth: WebAuth;
	
	public idToken?: string;
	public profile?: ProfileData;
	private tokenExpiry?: Date;
	
	constructor(auth: WebAuth) {
		super();
		this.auth = auth;
	}
	
	login(state?: any): void {
		this.auth.authorize({appState: state} as any)
	}
	
	async handleAuthentication(): Promise<string> {
		return new Promise((resolve, reject) => {
			this.auth.parseHash((err: Auth0ParseHashError | null, result: Auth0DecodedHash | null) => {
				if (err) reject(err);
				else {
					this.localLogin(result!);
					resolve(result!.idToken);
				}
			});
		});
	}
	
	localLogin(authResult: Auth0DecodedHash) {
		this.idToken = authResult.idToken;
		this.profile = authResult.idTokenPayload;
		this.tokenExpiry = new Date(this.profile!.exp * 1000);
		localStorage.setItem(this.localStorageKey, 'true');
		this.emit(this.loginEvent, {
			loggedIn: true,
			profile: authResult.idTokenPayload,
			state: authResult.appState || {}
		});
	}
	
	renewTokens(): Promise<any> {
		return new Promise((resolve, reject) => {
			if (localStorage.getItem(this.localStorageKey) !== "true") {
				return reject("Not logged in");
			}
			this.auth.checkSession({}, (err: Auth0Error | null, authResult: any) => {
				if (err) reject(err);
				else {
					this.localLogin(authResult);
					resolve(authResult);
				}
			});
		});
	}
	
	logOut() {
		localStorage.removeItem(this.localStorageKey);
		this.idToken = undefined;
		this.profile = undefined;
		this.tokenExpiry = undefined;
		this.auth.logout({
			returnTo: window.location.origin
		});
		this.emit(this.loginEvent, {loggedIn: false});
	}
	
	isAuthenticated() {
		return (
			Date.now() < (this.tokenExpiry as any as  number) &&
			localStorage.getItem(this.localStorageKey) === 'true'
		);
	}
	
	
}

export {AuthService}
