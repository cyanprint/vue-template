interface ApiError {
	name: string;
	message: any;
	type: number;
}

export {ApiError}
