async function RegisterServiceWorker() {
    if ('serviceWorker' in navigator) {
        const registration = await navigator.serviceWorker.register('/flag~pwa~sw.js')
        console.log('SW registered: ', registration);

        /* ==============================
             Un comment for WebPush API
         ================================*/
        // const UrlB64ToUint8Array = function (base64String: string) {
        //     const padding = '='.repeat((4 - base64String.length % 4) % 4);
        //     const base64 = (base64String + padding)
        //         .replace(/\-/g, '+')
        //         .replace(/_/g, '/');
        //
        //     const rawData = window.atob(base64);
        //     const outputArray = new Uint8Array(rawData.length);
        //
        //     for (let i = 0; i < rawData.length; ++i) {
        //         outputArray[i] = rawData.charCodeAt(i);
        //     }
        //     return outputArray;
        // }
        // const reg = await navigator.serviceWorker.ready;
        // let sub = await reg.pushManager.getSubscription();
        // if (sub == null) {
        //     sub = await registration.pushManager.subscribe({
        //         userVisibleOnly: true,
        //         applicationServerKey: UrlB64ToUint8Array('BBb-O-VjwMMS-4VKgTP1i6FC67xAUdDVJR_ktzbloRpOyjVJLLeX3ypOa4tI3twvg2aaaRJVXRitpVBbLdFS_u0')
        //     });
        // }
        // console.log(JSON.stringify(sub));
    }
}

//Load Service Worker
async function LoadServiceWorker(box: { prompt: () => void }): Promise<void> {

    // Capure prompt
    window.addEventListener('beforeinstallprompt', (e: Event) => {
        e.preventDefault();
        box.prompt = e as any as (() => void)
    });
    
    // Register service worker
    window.addEventListener('load', RegisterServiceWorker);

}


export {
    LoadServiceWorker,
}