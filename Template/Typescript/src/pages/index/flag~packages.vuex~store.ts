import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		counter: 0,
	},
	mutations: {
		increment: (state, {amount}) => state.counter += amount
	},
	actions: {
		increment: (context, data) => context.commit('increment', {amount: data.amount})
	},
	getters: {
		amount: state => state.counter
	}
});
