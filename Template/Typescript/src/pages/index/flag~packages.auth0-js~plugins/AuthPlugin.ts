import {AuthService} from "../../../classLibrary/flag~packages.auth0-js~auth/AuthService";
import {auth} from "../init";

declare module "vue/types/vue" {

	interface Vue {
		$auth: AuthService;
	}
}

export default {
	install(Vue: any) {
		Vue.prototype.$auth = auth;
		
		Vue.mixin({
			created() {
				if (this.handleLoginEvent) {
					auth.addListener("loginEvent", this.handleLoginEvent);
				}
			},
			
			destroyed() {
				if (this.handleLoginEvent) {
					auth.removeListener("loginEvent", this.handleLoginEvent);
				}
			}
		});
	}
};
