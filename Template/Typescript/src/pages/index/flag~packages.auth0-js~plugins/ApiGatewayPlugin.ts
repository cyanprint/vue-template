import {ApiGateway} from "../../../classLibrary/flag~packages.auth0-js~auth/ApiGateway";

declare module "vue/types/vue" {
	interface Vue {
		$api: ApiGateway;
	}
}

export default function (api: ApiGateway) {
	return {
		install(Vue: any) {
			Vue.prototype.$api = api;
			Vue.mixin({
				created() {
					if (this.handleApiError) {api.addListener(api.eventKey, this.handleApiError);}
				},
				destroyed() {
					if (this.handleApiError) {api.removeListener(api.eventKey, this.handleApiError);}
				}
			});
		}
	}
};
