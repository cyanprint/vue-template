import {Core, Kore} from "@kirinnee/core"; //flag~move.@kirinnee/core~
import {ObjectX, Objex} from "@kirinnee/objex"; //flag~packages.@kirinnee/objex~
import {WebAuth} from "auth0-js"; //flag~packages.auth0-js~
import {AuthService} from "../../classLibrary/flag~packages.auth0-js~auth/AuthService";
import {ApiGateway} from "../../classLibrary/flag~packages.auth0-js~auth/ApiGateway";
import authConfig from "../../../config/flag~packages.auth0-js~auth_config.json";

const core: Core = new Kore();//flag~move.@kirinnee/core~
core.ExtendPrimitives();//flag~move.@kirinnee/core~

// @ts-ignore flag~packages.@kirinnee/objex~
const objex: Objex = new ObjectX(core); //flag~packages.@kirinnee/objex~
objex.ExtendPrimitives(); //flag~packages.@kirinnee/objex~

//if~packages.auth0-js~
const config: { domain: string, clientId: string } = PRODUCTION ? authConfig.prod : authConfig.dev;

const webAuth: WebAuth = new WebAuth({
    domain: config.domain,
    redirectUri: `${window.location.origin}/callback`,
    clientID: config.clientId,
    responseType: 'id_token',
    scope: 'openid email profile'
});
const auth: AuthService = new AuthService(webAuth);
const host: string = PRODUCTION ? "var~auth.api.domain~" : "http://localhost:3001";
const gateway: ApiGateway = new ApiGateway(host, auth);
//end~packages.auth0-js~

const $$ = (i: number): Promise<void> => new Promise<void>(r => setTimeout(r, i));
const isMobile = (): boolean => window.innerHeight > window.innerWidth;
const isPWA = (): boolean => window.matchMedia('(display-mode: standalone)').matches; //flag~pwa~

export {
    $$,
    isMobile,
    isPWA, //flag~pwa~
    core,//flag~move.@kirinnee/core~
    objex, //flag~packages.@kirinnee/objex~
    auth, gateway, //flag~packages.auth0-js~
}
