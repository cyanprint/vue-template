import Vue from 'vue';
import App from './App.vue';
import './flag~build.sass~index.scss';
import './flag~build.style~index.css';
import store from "./flag~packages.vuex~store"; //flag~packages.vuex~
import {router} from "./flag~packages.vue-router~router";
import {images} from './images';
import AuthPlugin from "./flag~packages.auth0-js~plugins/AuthPlugin"; //flag~packages.auth0-js~
import ApiGatewayPlugin from "./flag~packages.auth0-js~plugins/ApiGatewayPlugin"; //flag~packages.auth0-js~
import {gateway} from "./init"; //flag~packages.auth0-js~
import {LoadServiceWorker} from "./flag~pwa~service-worker";

Vue.config.productionTip = false;
//if~packages.auth0-js~
Vue.use(AuthPlugin);
Vue.use(ApiGatewayPlugin(gateway));
//end~packages.auth0-js~

const box = {
    prompt: () => {
    }
};

if (PRODUCTION) {
    LoadServiceWorker(box).then(() => console.log("Service worker loaded"));
}


new Vue({
    store, //flag~packages.vuex~
    router, //flag~packages.vue-router~
    render: h => h(App)
}).$mount('#app');

export {
    images,
    box, //flag~pwa~
}
