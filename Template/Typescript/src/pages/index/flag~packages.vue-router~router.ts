import Vue from 'vue'
import Router, {Route, RouteConfig} from 'vue-router'
import HelloWorld from "./components/HelloWorld.vue";
import About from "./views/flag~packages.vue-router~About.vue";
import NotFound from "./views/flag~packages.vue-router~NotFound.vue";

import Profile from "./views/flag~packages.auth0-js~Profile.vue";
import VerifyEmail from "./views/flag~packages.auth0-js~VerifyEmail.vue";
import Callback from "./components/flag~packages.auth0-js~Callback.vue";
import {auth} from "./init"; //flag~packages.auth0-js~

Vue.use(Router);

const routes: RouteConfig[] = [
	{path: '/', name: 'home', component: HelloWorld, meta: {public: true, private: true}},
	{path: '/about', name: 'about', component: About, meta: {public: true, private: true}},
	{path: '*', name: '404', component: NotFound, meta: {public: true, private: true}},
	
	//if~packages.auth0-js~
	{path: '/callback', name: 'callback', component: Callback, meta: {public: true, private: true}},
	{path: '/profile', name: 'profile', component: Profile, meta: {public: false, private: true}},
	{path: '/verify_email', name: 'verify_email', component: VerifyEmail, meta: {public: false, private: true}}
	//end~packages.auth0-js~
];
const router: Router = new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

//Middle Ware
router.beforeEach((to: Route, from: Route, next) => {
	//if~packages.auth0-js~
	const guard: { public: boolean, private: boolean } = to.meta;
	if (to.path === "/callback") return next();
	if (guard.public && !auth.isAuthenticated()) return next();
	if (guard.private && auth.isAuthenticated()) {
		if (!guard.public && !auth.profile!.email_verified && to.path !== "/verify_email") {
			router.push("/verify_email");
			return;
		} else {
			//end~packages.auth0-js~
			return next();
			//if~packages.auth0-js~
		}
	}
	auth.login({target: to.path});
	//end~packages.auth0-js~
});

export {router};
