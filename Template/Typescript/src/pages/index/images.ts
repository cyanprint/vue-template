import logo from "./assets/logo.png"
import {Rimage, Rimager} from "@kirinnee/rimage"; //flag~packages.@kirinnee/rimage~
import {SortType} from "@kirinnee/core"; //flag~move.@kirinnee/core~flag~packages.@kirinnee/rimage~
import {core} from "./init"; //flag~move.@kirinnee/core~

core.AssertExtend();//flag~move.@kirinnee/core~

let images: any = {
	logo
};

//if~packages.@kirinnee/rimage~
declare var PRODUCTION: boolean;
const imageDeployConfig: any = require("../../../config/image.deploy.config.json")[0];

const config: Rimage = {
	key: imageDeployConfig.key,
	width: imageDeployConfig.width,
	sizes: imageDeployConfig.sizes
};

let rimager: Rimager = new Rimager(core, config, new SortType(), !PRODUCTION);
rimager.ExtendPrimitives();

images = rimager.RegisterImages(images);
//end~packages.@kirinnee/rimage~

export {
	images,
	rimager, //flag~packages.@kirinnee/rimage~
}
