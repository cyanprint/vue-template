module.exports = {
    "roots": [
        "<rootDir>"
    ],
    "transform":{
        "^.+\\.tsx?$": "ts-jest"
    },
    "moduleNameMapper": { //flag~build.style~
        "\\.(css|scss)$": "identity-obj-proxy" //flag~build.style~
    },//flag~build.style~
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
    collectCoverageFrom: [
        "**/src/classLibrary/**/*.ts"
    ],

    "coverageThreshold": {
        "global": {
            "branches": 80,
            "functions": 80,
            "lines": 80,
            "statements": 100
        }
    }
};