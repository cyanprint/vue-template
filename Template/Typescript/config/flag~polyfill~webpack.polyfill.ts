const polyfill:string[] = [
    "@babel/polyfill", //flag~packages.@babel/polyfill~
    "whatwg-fetch", //flag~packages.whatwg-fetch~
    "dom4" //flag~packages.dom4~
];

export {polyfill}; 