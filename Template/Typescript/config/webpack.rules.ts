import {RuleSetRule, RuleSetUseItem} from "webpack";
import * as path from "path"; //flag~build.babel~
import {prodFileOpts} from "./config.prod";
import {devFileOpts} from "./config.dev";

//if~build.style~
/*===================
 STYLE LOADER
 ===================== */
const preCSSLoaders: RuleSetUseItem[] = [
	{loader: 'postcss-loader', options: {config: {path: `${__dirname}/postcss.config.js`}}}, //flag~build.postcss~
	"sass-loader" //flag~build.sass~
];

const use: RuleSetUseItem[] = [
	"style-loader",
	{loader: "css-loader", options: {importLoaders: preCSSLoaders.length}}
];

const cssRule: RuleSetRule = {
	test: /\.s?css/,
	use: use.concat(preCSSLoaders)
};
//end~build.style~


//if~build.babel~
/*===================
 BABEL LOADER
 ===================== */
const babelLoader: RuleSetUseItem = {
	loader: 'babel-loader',
	options: {
		configFile: path.resolve(__dirname, "./flag~build.babel~babel.config.js")
	}
};

//if~pwa~
const babelWorkBoxLoader: RuleSetUseItem = {
	loader: 'babel-loader',
	options: {configFile: path.resolve(__dirname, "./flag~build.babel~flag~pwa~babel-workbox.config.js")}
}
//end~pwa~


const babel: RuleSetRule = {
	test: /\.jsx?$/,
	exclude: /workbox/,
	use: [
		babelLoader
	]
};
//end~build.babel~

/*===================
 TS LOADER
 ===================== */

const tsLoader: RuleSetUseItem = {
	loader: 'ts-loader', options: {
		transpileOnly: true,
		appendTsSuffixTo: [/\.vue$/],
	}
};

const typescript: RuleSetRule = {
	test: /\.tsx?$/,
	exclude: /(node_modules|bower_components|sw\.ts)/,
	use: [
		babelLoader, //flag~build.babel~
		tsLoader,
	]
}

//if~pwa~
const workbox: RuleSetRule = {
	test: /sw\.ts$/,
	exclude: /(node_modules|bower_components)/,
	use: [
		babelWorkBoxLoader, //flag~build.babel~
		tsLoader,
	]
}
//end~pwa~

/*===================
 Vue LOADER
 ===================== */

const vue: RuleSetRule = {
	test: /\.vue$/, loader: 'vue-loader',
	options: {hotReload: true} //flag~build.hmr~
};

/*===================
 File LOADER
 ===================== */

function file(env: "production" | "development" | "none"): RuleSetRule {
	return {
		exclude: [/\.vue$/, /\.(js|jsx|mjs)$/, /\.(ts|tsx)$/, /\.(scss|css)$/, /\.html$/, /\.json$/],
		loader: 'file-loader',
		options: env === "production" ? prodFileOpts : devFileOpts
	}
}
/* ==============
 Combining
 ================= */

function rules(env: "production" | "development" | "none"): RuleSetRule[] {
	return [
		vue,
		typescript,
		workbox, //flag~pwa~
		babel, //flag~build.babel~
		cssRule, //flag~build.style~
		file(env)
	];
}

export {rules};
