import UglifyJsPlugin from 'uglifyjs-webpack-plugin'; //flag!~build.es6~
import webpack, {Options} from "webpack";
import SplitChunksOptions = webpack.Options.SplitChunksOptions; //flag~build.split~
const TerserPlugin = require('terser-webpack-plugin'); //flag~build.es6~


// @ts-ignore
const plugins: Array<Plugin | Tapable.Plugin> = [

	//if~build.es6~
	new TerserPlugin({
		terserOptions: {
			compress: {
				drop_console: false,
				unsafe: true
			},
			output: {comments: false},
			toplevel: true
		}
	}),
	//end~build.es6~

	//if!~build.es6~
	new UglifyJsPlugin({
		uglifyOptions: {
			compress: {
				drop_console: false,//flag!~build.console~
				unsafe: true //flag~build.unsafe~
			},
			output: {comments: false},//flag~build.stripComment~
			toplevel: true //flag~build.renameGlobal~
		}
	})
	//end!~build.es6~
]


//if~build.split~
const chunkOpts: SplitChunksOptions = {
	name: true,
	chunks: 'async',
	// minSize: 20000,
	// maxSize: 250000,
	cacheGroups: {
		vue: {
			test: /[\\/]node_modules[\\/]vue/,
			priority: -10,
			name: 'vue',
			chunks: 'all'
		},
		library: {
			test: /[\\/]node_modules[\\/]/,
			priority: -20,
			name: 'vendors',
			chunks: 'all'
		}
	}
};
//end~build.split~

function optimize(	minimize: boolean
					  ,chunkSplit: boolean //flag~build.split~
) :Options.Optimization {
	let opti : Options.Optimization = {};
	if(minimize) opti.minimizer = plugins;
	if(chunkSplit) opti.splitChunks = chunkOpts; //flag~build.split~
	return opti;
}

export {optimize};