module.exports = {
    extension: ['ts'],
    require: [
        'ts-node/register',
    ],
    recursive: true,
    spec: './test/flag~packages.chai~unit/**/*.spec.ts'
};