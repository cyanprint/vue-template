let spawn = require('child_process').spawn;
let rimraf = require("rimraf"); //flag~packages.nyc~
let fs = require("fs");

//command will be the first argument
let command = process.argv[2];
//args will be the array of arguments after the commands
let args = process.argv.slice(3).map(s => s.substr(2) === "--" ? s.substr(1) : s).filter(s => s !== '-w' && s !== '-watch');
//This is a boolean as to whether --watch or -w been in the commands
let watch = process.argv.map(s => s.substr(2) === "--" ? s.substr(1) : s).filter(s => s === '-w' || s === "-watch").length > 0;

Execute(command, args, watch).then();

//The decision on what command to execute base on the arguments and whether watch exists
async function Execute(command, args, watch) {
    let coverage = args.filter(s => s === "--cover").length > 0; //flag~cover~
    let full = args.filter(s => s === "--full").length > 0;
    switch (command) {
        case "wp":
            let wp = `webpack --config ./config/webpack.${args[0]}.ts`;
            if (watch) wp += " --watch";
            await run(wp);
            break;
        //if~packages.testcafe~
        case "flag~packages.testcafe~e2e":
            await run("node script wp prod");
            let headless = [
                "chrome:headless", //flag~e2e.chrome.headless~
                "firefox:headless", //flag~e2e.firefox.headless~
                "chrome-canary:headless" //flag~e2e.chrome-canary.headless~
            ].map(s => `testcafe ${s} ./test/flag~packages.testcafe~e2e/**/*.test.ts`);
            let real = [
                "chrome", //flag~e2e.chrome.real~
                "firefox", //flag~e2e.firefox.real~
                "ie", //flag~e2e.ie~
                "opera", //flag~e2e.opera~
                "chrome-canary", //flag~e2e.chrome-canary.real~
                "edge" //flag~e2e.edge~
            ].map(s => `testcafe ${s} ./test/flag~packages.testcafe~e2e/**/*.test.ts`);
            let test = headless;
            if (full) test = test.concat(real);
            await run(test.join(" && "));
            break;
        //end~packages.testcafe~
        //if~packages.testcafe~
        //if~gitlab~
        case "cie2e":
            await run("node script wp prod");
            await run(`testcafe "chrome:headless --no-sandbox" ./test/e2e/**/*.test.ts`); //flag~e2e.chrome.headless~
            await run(`testcafe firefox:headless ./test/e2e/**/*.test.ts`); //flag~e2e.firefox.headless~
            break;
        //end~gitlab~
        //end~packages.testcafe~
        //if~packages.chai~
        case "flag~packages.chai~unit":
            let mocha = `mocha --config ./config/flag~packages.chai~.mocharc.js`;
            if (watch) mocha += " --watch-extensions ts --watch ";
            if (coverage) mocha = "nyc --nycrc-path ./config/flag~packages.nyc~.nycrc " + mocha; //flag~cover~
            if (fs.existsSync("./.nyc_output")) rimraf.sync("./.nyc_output"); //flag~cover~
            await run(mocha);
            break;
        //end~packages.chai~
        //if~packages.jest~
        case "flag~packages.jest~unit":
            let jest = "jest --rootDir --config=./config/flag~packages.jest~jest.config.js ./test/flag~packages.jest~unit/";
            if (watch) jest += " --watch";
            if (coverage) jest += " --coverage";  //flag~cover~
            await run(jest);
            break;
        //end~packages.jest~
        default:
            console.log("Unknown command!");
            process.exit(1);
    }

}

//Executes the function as if its on the CMD. Exits the script if the external command crashes.
async function run(command) {
    if (!Array.isArray(command) && typeof command === "string") command = command.split(' ');
    else throw new Error("command is either a string or a string array");
    let c = command.shift();
    let v = command;

    let env = process.env;
    env.BROWSERSLIST_CONFIG= "./config/.browserslistrc";


    return new Promise((resolve) => spawn(c, v,
        {
            stdio: "inherit",
            shell: true,
            env: env
        })
        .on("exit", (code, signal) => {
            if (fs.existsSync("./.nyc_output")) rimraf.sync("./.nyc_output"); //flag~cover~
            if (code === 0) resolve();
            else {
                console.log("ExternalError:", signal);
                process.exit(1);
            }
        })
    );
}