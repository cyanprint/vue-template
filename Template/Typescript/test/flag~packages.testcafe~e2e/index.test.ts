import {Selector} from 'testcafe';

import {should} from 'chai';//flag~packages.chai~
should();//flag~packages.chai~

fixture`Index Page`.page`../../dist/index.html`;

test('Check if reversing input works', async t => {
	await t
		.typeText('input', 'i am kirinnee');
	
	const value = await Selector(".message").textContent;
	
	await t.expect(value).eql("eennirik ma i test2"); //flag!~packages.chai~
	value.should.equal("eennirik ma i test2"); //flag~packages.chai~
});