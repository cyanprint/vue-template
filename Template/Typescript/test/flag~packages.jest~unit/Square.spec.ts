import {Dog} from "../../src/classLibrary/Dog";

// @ts-ignore
test("Dog", () => {
	const korgi: Dog = new Dog();
	expect(korgi.bark()).toBe("woof");
});