import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);
const Foo = {template: '<div>foo</div>'};
const Home = {template: '<div>home</div>'};

const routes = [
    {path: '/home', component: Home},
    {path: '/foo', component: Foo}
];

const router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

//Middle Ware
router.beforeEach((to, from, next) => {
    return next();
});

export {router};
