import Vue from 'vue';
import './flag~build.sass~index.scss';
import './flag~build.style~index.css';
import store from "./flag~packages.vuex~store"; //flag~packages.vuex~
import _ from 'lodash'; //flag~packages.lodash~
import {router} from "./flag~packages.vue-router~router";
import App from "./App";
import {deferredPrompt} from "./flag~pwa~service-worker";


Vue.config.productionTip = false;

new Vue({
    store, //flag~packages.vuex~
    router, //flag~packages.vue-router~
    render: h => h(App)
}).$mount('#app');

export {
    deferredPrompt //flag~pwa~
}