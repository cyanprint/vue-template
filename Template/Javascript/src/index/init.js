import {Kore} from "@kirinnee/core"; //flag~move.@kirinnee/core~
import {ObjectX} from "@kirinnee/objex"; //flag~packages.@kirinnee/objex~

const core = new Kore();//flag~move.@kirinnee/core~
core.ExtendPrimitives();//flag~move.@kirinnee/core~

const objex = new ObjectX(core); //flag~packages.@kirinnee/objex~
objex.ExtendPrimitives(); //flag~packages.@kirinnee/objex~

export {
    core, //flag~move.@kirinnee/core~
    objex //flag~packages.@kirinnee/objex~
}