import logo from "./assets/logo.png";
import {Rimager} from "@kirinnee/rimage";//flag~packages.@kirinnee/rimage~
import {SortType} from "@kirinnee/core"; //flag~move.@kirinnee/core~flag~packages.@kirinnee/rimage~
import {core} from "./init"; //flag~move.@kirinnee/core~

core.AssertExtend();

let images = {
    logo
};

//if~packages.@kirinnee/rimage~
let imageDeployConfig = require("../../config/image.deploy.config.json")[0];

const config = {
    key: imageDeployConfig.key,
    width: imageDeployConfig.width,
    sizes: imageDeployConfig.sizes
};

const rimager = new Rimager(core, config, new SortType(), !PRODUCTION);
rimager.ExtendPrimitives();

images = rimager.RegisterImages(images);
//end~packages.@kirinnee/rimage~


export {
    images,
    rimager  //flag~packages.@kirinnee/rimage~
}