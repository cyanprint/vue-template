const prodFileOpts = {
    name: '[path][name].[ext]',
    publicPath: 'var~rimage.publicPath~',
    context: './src'
};

module.exports = prodFileOpts;
