const path = require("path"); //flag~build.babel~
const prodFileOpts = require("./config.prod");
const devFileOpts = require("./config.dev");

//if~build.style~
/*===================
 STYLE LOADER
 ===================== */
const preCSSLoaders = [
    {loader: 'postcss-loader', options: {config: {path: `${__dirname}/postcss.config.js`}}}, //flag~build.postcss~
    "sass-loader" //flag~build.sass~
];

const use = [
    "style-loader",
    {loader: "css-loader", options: {importLoaders: preCSSLoaders.length}}
];

const cssRule = {
    test: /\.s?css/,
    use: use.concat(preCSSLoaders)
};
//end~build.style~


/*===================
 Babel LOADER
 ===================== */

const uses = [
    {loader: 'babel-loader', options: {configFile: path.resolve(__dirname, "./flag~build.babel~babel.config.js")}}, //flag~build.babel~
];

const scripts = {
    test: /\.jsx?$/,
    exclude: /(node_modules|bower_components)/,
    use: uses
};
/*===================
 Vue LOADER
 ===================== */

const vue = {
    test: /\.vue$/, loader: 'vue-loader',
    options: {hotReload: true} //flag~build.hmr~
};

/*===================
 File LOADER
 ===================== */

function file(env) {
    return {
        exclude: [/\.vue$/, /\.(js|jsx|mjs)$/, /\.(ts|tsx)$/, /\.(scss|css)$/, /\.html$/, /\.json$/],
        loader: 'file-loader',
        options: env === "production" ? prodFileOpts : devFileOpts
    }
}


/* ==============
 Combining
 ================= */

function rules(env) {
    return [
        vue,
        scripts,
        cssRule, //flag~build.style~
        file(env)
    ];
}

module.exports = rules;
