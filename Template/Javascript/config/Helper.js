const path = require("path");

module.exports = function ConvertToOption(p) {
    let opts = {
        title: p.title || "Index",
        filename: p.output || "index.html",
        chunks: p.chunks || ["index"]
    };
    if (p.template) opts.template = path.join("./public", p.template);
    return opts;
}