module.exports = {
    chunks: [
        ["index", "./src/index/index.js"]
    ],
    pages: [
        {
            template: "index.html",
            output: "index.html",
            chunks: ['index'],
            title: 'Index'
        }
    ],
};

