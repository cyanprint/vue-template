const path = require("path");
const opti = require("./webpack.optimizer");
const webpack = require("webpack");
const rules = require("./webpack.rules");
const Kore = require("@kirinnee/core").Kore;
require("webpack-dev-server");
const pages = require("./pages");
const ConvertToOption = require("./Helper");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const WorkboxPlugin = require("workbox-webpack-plugin"); //flag~packages.workbox-webpack-plugin~

const core = new Kore();
core.ExtendPrimitives();

// Load Pages as HTML plugin
const htmlPages = pages.pages
    .Map(s => ConvertToOption(s))
    .Map(s => new HtmlWebpackPlugin(s));

// Additional common plugins
const vuePlugin = new VueLoaderPlugin();
const cleanPlugin = new CleanWebpackPlugin({
    cleanOnceBeforeBuildPatterns: [
        "**/*.*",
        '!.gitignore',
    ]
});
const copyPlugin = new CopyPlugin([{
    from: 'public',
    ignore: [".html"],
    to: ''
}]);

// All plugins
const commonPlugins = [...htmlPages, vuePlugin, cleanPlugin, copyPlugin];

function GenerateConfig(entry, filename, mode) {
    const outDir = path.resolve(__dirname, "../dist");
    const globals = new webpack.DefinePlugin({
        PRODUCTION: JSON.stringify(mode === "production")
    });
    //if~pwa~
    const workbox = new WorkboxPlugin.GenerateSW({
        skipWaiting: true,
        clientsClaim: true,
        include: [
            /\.(js|css|html|json|png|jpg|jpeg|svg)?$/
        ],
        runtimeCaching: [
            {
                urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
                handler: "CacheFirst",
            }
        ]
    });
    //end~pwa~
    const envPlugins = mode === "production" ?
        // Production
        [
            workbox, //flag~pwa~
        ]


        :

        // Development
        [
            new webpack.HotModuleReplacementPlugin(), //flag~build.hmr~
        ];

    const plugins = [...commonPlugins, ...envPlugins, globals];
    const config = {
        entry,
        output: {
            path: outDir,
            filename,
            publicPath: '/',
            libraryTarget: "umd",
            globalObject: "(typeof window !== 'undefined' ? window : this)"
        },
        resolve: {
            extensions: ['.js', '.vue'],
            alias: {'vue$': 'vue/dist/vue.esm.js'} //flag~build.includeVueCompiler~
        },
        mode,
        devtool: "source-map", //flag~build.sourceMap~
        module: {rules: rules(mode)},
        plugins,
    };
    config.target = "web";
    if (mode === "production") config.optimization = opti;
    else if (mode === "development") {
        config.devServer = {
            contentBase: path.resolve(__dirname, '../dist'),
            hot: true, //flag~build.hmr~
            historyApiFallback: true, //flag~packages.vue~router~
        }
    }
    return config;
}

module.exports = GenerateConfig;
