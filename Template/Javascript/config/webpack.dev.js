const Kore = require("@kirinnee/core").Kore;
const pages = require("./pages");
const GenerateConfig = require("./webpack.base");

const core = new Kore();
core.ExtendPrimitives();

const entry = new Map(pages.chunks)
    .MapValue(v => core.WrapArray(v))
    .AsObject();

module.exports = GenerateConfig(entry, '[name].js', 'development');
