const devFileOpts = {
    name: '[path][name].[ext]',
    publicPath: '',
    context: './src'
};

module.exports = devFileOpts;
