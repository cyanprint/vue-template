const Kore = require("@kirinnee/core").Kore;
const polyfill = require("./flag~polyfill~webpack.polyfill");
const pages = require("./pages");
const GenerateConfig = require("./webpack.base");

const core = new Kore();
core.ExtendPrimitives();

//Add polyfill to each chunk if there is polyfill! flag~polyfill~
const entry = new Map(pages.chunks)
    .MapValue(v => core.WrapArray(v))
    .MapValue(v => polyfill.concat(v)) //flag~polyfill~
    .AsObject();
module.exports = GenerateConfig(entry, '[name].[contenthash].js', 'production');
