import {Selector} from 'testcafe';

require("chai").should();//flag~packages.chai~

fixture`Index Page`.page`../../dist/index.html`;

test('Check if reversing input works', async t => {
    await t
        .typeText('input', 'i am kirinnee');

    let value = await Selector(".message").textContent;
    value = value.trim();
    await t.expect(value).eql("eennirik ma i test2"); //flag!~packages.chai~
    value.should.equal("eennirik ma i test2"); //flag~packages.chai~
});