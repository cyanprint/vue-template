let Dog = require("../../src/classLibrary/Dog");
require("chai").should();

describe("Dog", () => {
    let korgi = new Dog();
    it("should return woof when bark", () => {
        korgi.bark().should.equal("woof");
    });

});
